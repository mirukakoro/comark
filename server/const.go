package server

type Page struct {
	Title string
	Body  []byte
}
