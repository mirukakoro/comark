package server

import (
	"html/template"
	"net/http"
	"bytes"
	"time"
	"bufio"
)

type Template struct {
	path string
}

type layoutData struct {
	Content string
	GenTime string // ISO8601
}

func NewTemplate(path string) Template {
	return Template{
		path: path,
	}
}

var templates = template.Must(template.ParseFiles("edit.html", "view.html"))

func (t *Template) Render(w http.ResponseWriter, path string, p *Page) {
	var contentBuf bytes.Buffer
	err := templates.ExecuteTemplate(bufio.NewWriter(&contentBuf), path, p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	content := layoutData{
		Content: contentBuf.String(),
		GenTime: time.Now().Format(time.RFC3339),
	}
	err = templates.ExecuteTemplate(w, t.path, content)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}