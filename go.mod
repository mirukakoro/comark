module gitlab.com/colourdelete/comark

go 1.15

require (
	github.com/yuin/goldmark v1.2.1
	github.com/yuin/goldmark-meta v1.0.0
)
