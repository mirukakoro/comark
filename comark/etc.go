package comark

import (
	homedir "github.com/mitchellh/go-homedir"
	"os"
)

func CreateOwnDirIfNotExists() (string, error) {
	rawDir, err := homedir.Dir()
	if err != nil {
		return "", err
	}
	path, err := homedir.Expand(rawDir)
	if err != nil {
		return "", err
	}
	if _, err := os.Stat(path); os.IsNotExist(err) {
		os.MkdirAll(path, os.ModeDir)
	}
	return path, nil
}
