package comark

import (
	git "github.com/go-git/go-git/v5"
	"github.com/google/uuid"
	"path"
	"os"
)

func CloneRepo(url string) (string, *git.Repository, error) {
	homePath, err := CreateOwnDirIfNotExists()
	if err != nil {
		return "", nil, err
	}
	repoPath := path.Join(homePath, uuid.New().String())
	repo, err := git.PlainClone(repoPath, false, &git.CloneOptions{
		URL:      "https://github.com/go-git/go-git",
		Progress: os.Stdout,
	})
	return repoPath, repo, nil
}

func CloneRepoMem(url string) (*git.Repository, error) {
	repo, err := git.Clone(memory.NewStorage(), nil, &git.CloneOptions{
		URL:      "https://github.com/go-git/go-git",
		Progress: os.Stdout,
	})
	return repo, nil
}

func OpenRepo(path string) (*git.Repository, error) {
	return git.PlainOpen(path)
}

func UpdateRepo(repo *git.Repository) (error) {
	worktree, err := repo.Worktree()
	if err != nil {
		return err
	}
	worktree.Pull(&git.PullOptions{})
	repo.Push(&git.PushOptions{
		Progress: os.Stdout,
	})
	return nil
}
